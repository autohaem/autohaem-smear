# Oilite bushing

* Plain bushing
* 6mm inner diameter.
* 9mm outer diameter.
* 16mm length.

Available from e.g. [HPC gears](https://www.hpcgears.com/n/products/shafts_bearings/2.oilite/oilite_plain/oilite_plain.php)