# Rods

Two rods are required:

* 4mm diameter
* 100mm length
* Ground steel is preferred

You might need to buy them longer and cut to length. Available from many metal suppliers and Amazon/eBay.

e.g. HPC Gears on [eBay](https://www.ebay.co.uk/itm/111691087858)