# Rod

* Ground steel rod.
* 6mm diameter.
* 100mm long.

Available from e.g. [HPC gears](https://www.ebay.co.uk/itm/111690270010).