# Magnet

* 2.5 * 2.5 * 7 mm
* Neodymium magnet

Available from e.g. [First4Magnets](https://www.first4magnets.com/rectangular-c35/2-5-x-7-x-2-5mm-thick-n42-neodymium-magnet-0-4kg-pull-p3047#ps_0_3094|ps_1_1057).