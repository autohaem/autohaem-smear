# Linear bearings

These are typically known as LM4UU and should have:

* Inner diameter: 4mm
* Outer diameter: 8mm
* Length: 12mm

e.g. Amazon [8438508025904](https://www.amazon.co.uk/LM4UU-Rubber-Shielded-Linear-Bearings/dp/B07YSS7WT4) 