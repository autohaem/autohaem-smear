# Operation

## Place microscope slide in slot {pagestep}

![](../images/operation/1.JPG)

## Place drop of blood on microscope slide {pagestep}

![](../images/operation/2.JPG)
![](../images/operation/3.JPG)

## Insert spreader slide into slider slot {pagestep}

![](../images/operation/4.JPG)

## Pull back slider {pagestep}

![](../images/operation/5.JPG)

## Push slider forward {pagestep}

![](../images/operation/6.JPG)

## Lift up slider and remove microscope slide with smear {pagestep}

![](../images/operation/7.JPG)

## Remove spreader slide {pagestep}

![](../images/operation/8.JPG)

