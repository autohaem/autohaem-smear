# Assembly

{{BOM}}

[main body]: ../models/smear_main_body.stl "{cat:3Dprinted}"
[hinge component]: ../models/hinge_component.stl "{cat:3DPrinted}"
[slider]: ../models/slider.stl "{cat:3DPrinted}"
[rod handle]: ../models/rod_handle.stl "{cat:3DPrinted}"

[M3x10mm screw]: "{cat:part}"
[M3 nut]: "{cat:part}"
[100mm ground steel rod]: ../components/rod.md "{cat:part}"
[Oilite bushing]: ../components/bushing.md "{cat:part}"
[light oil]: "{cat:part}"
[magnet]: ../components/magnet.md "{cat:part}"

[M2.5 hex head screwdriver]: "{cat:tool}"

# Method
## Attach hinge component {pagestep}

Attach the [hinge component]{qty:1} to the [main body]{qty:1} with using two [M3x10mm screw]{qty:2}s and [M3 nut]{qty:2}s with a [M2.5 hex head screwdriver]{qty:1}.

![](../images/assembly/raw_base.jpg)
![](../images/assembly/nuts.jpg)
![](../images/assembly/hinge_component.jpg)

## Attach rods {pagestep}

Push the [100mm ground steel rod]{qty:2}s into the hinge component.  If they are not tight, use some [glue]{qty:some}.

![](../images/assembly/rods.jpg)

## Attach magnets {pagestep}

Use [glue]{qty:some} to attach two [magnet]{qty:2}s in the cavities on the outer sides of the rod supports.

![](../images/assembly/magnets.jpg)

## Insert slider's linear bushings {pagestep}

Push the [Oilite bushing]{qty:2}s through the holes in the [slider].

![](../images/assembly/bearings_before.jpg)
![](../images/assembly/bearings_after.jpg)

## Insert slider {pagestep}

Slide the [slider]{qty:1} onto the [rod]s, making sure that the linear bushings are correctly in place.

![](../images/assembly/mounted_slider.jpg)

## Attach rod holder {pagestep}

Push the [rod handle]{qty:1} onto the rods. If it is not tight, use some [glue]{qty:some}.

![](../images/assembly/rod_holder.jpg)
