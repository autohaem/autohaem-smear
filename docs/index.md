# autohaem smear

autohaem smear is a simple mechanical device for producing semi-automated blood smears.

![](images/autohaem_smear.JPG)
## Printing guidance

We normally print with PLA using a 0.15 layer height and ~18% infill. You should not need to print any of the parts with adhesion settings (brim etc.) or supports.

## Instructions

The [bill of materials]{BOM} can also be downloaded as an CSV.

* [Assembling autohaem smear](pages/assembly.md){step}
* [Operating autohaem smear](pages/operation.md){step}
