# autohaem smear

A mechanical 3D printed device for automated blood smears.

![Photo of autohaem smear](images/autohaem%20smear%20title%20card.jpg)

## Printing and building it

Go to the [autohaem smear webpage](https://autohaem.org/smear/) to download the STLs and follow the assembly instructions.

## Customising it

All of the designs are made using [OpenSCAD](https://www.openscad.org/). Currently we use [OpenSCAD 2019.05](https://openscad.org/news.html#20190518).

### Cloning the repository

The autohaem smear repository uses additional git features which can require extra steps. For this reason, it is recommended that you clone it, rather than download it.  

* The repository uses [git-lfs](https://git-lfs.github.com/) for version control of image (and other large) files. If git-lfs is not installed, cloning the repository will only download placeholders of these files.

#### Visual Studio Code

The easiest way to clone the repository is with [Visual Studio (VS) Code](https://code.visualstudio.com/), with [git-lfs installed](https://git-lfs.github.com/).  If you [clone the repository using VS Code](https://code.visualstudio.com/docs/editor/versioncontrol#_cloning-a-repository) these additional features should work automatically.

You can then make changes using VS Code and use OpenScad with the editor hidden and the `automatic reload and compile` option ticked for compiling.

#### Cloning using the command line

1. Download and install [git-lfs](https://git-lfs.github.com/).

2. The easiest way to correctly clone the project is to run:

    ```bash
    git clone https://gitlab.com/autohaem/autohaem-smear.git
    ```

 > With git-lfs installed, git will download the latest version of the large files.  If they are still missing run:

```bash
git lfs fetch 
git lfs checkout
```

  > To make Git always download everything in the repository run the following commands in your terminal:

```bash
git config --local lfs.fetchexclude ""
git lfs fetch
git lfs checkout
```

### Downloading the repository

1. Download the `autohaem smear` repository.

### Guide to repository

The autohaem smear components are found in the `openscad` folder. For example the main body is compiled from `openscad/main_body.scad`. The shared parameters for both devices resdie in `openscad/shared_parameters.scad`.  The parameters for autohaem smear reside in `openscad/smear_parameters.scad`, so you can easily change aspects of the design (height etc.).

#### Building the components

To build every component you can run ``build.sh`` if you have a Bash shell (in either linux or Windows Subsystem Linux).  You may need to add ``openscad`` to your path if you want this to work in MinGW on Windows, or in the Mac OS Terminal.

It is also useful to look in ``build.sh`` to see how the components are compiled (and from which OpenSCAD file) as some use command line variables.

To build individual components on the command line, you can run the following:

```bash
openscad -o builds/smear_main_body.stl openscad/smear_main_body.scad
```
