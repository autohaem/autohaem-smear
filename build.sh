#!/bin/bash

mkdir -p builds

openscad -o builds/smear_main_body.stl openscad/smear_main_body.scad
openscad -o builds/rod_handle.stl openscad/rod_handle.scad
openscad -o builds/slider.stl openscad/slider.scad
openscad -o builds/hinge_component.stl openscad/hinge_component.scad