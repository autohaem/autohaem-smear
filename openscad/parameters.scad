version_numstring ="v2.0.0";

//Main box
radius_of_curvature = 1;
box_size = [110,50,40];



//Wedge sides
wedge_length = box_length;
wedge_width = 10;
wedge_height = box_height;

//Microscope slide
microscope_slide_bodge_factor = 0.5;
microscope_slide_size = [76,26,1];
microscope_slide_offset = 3;


//Living hinge
hinge_box_size  = [25,box_size.y,16];
hinge_box_screws = microscope_slide_size.y/2;


//Rod rest
rod_holder_size = [10,10,17];
rod_length=100;
rod_rest_size = [10,10,hinge_box_size.z/2];
rod_rest_cylinder_length = 50;
rod_rest_x_pos = microscope_slide_offset;
magnet_size = [7.2,2.7,2.5];


//Rod
rod_radius = 2;
rod_radius_slide_bodge = 0.25;
rod_radius_bodge = 0.05;
distance_between_rods = box_size.y-10;

// Rod Holder 
rod_holder_length = 7;
rod_holder_width = distance_between_rods + rod_holder_length;
rod_holder_height = 7;

// Slider
slider_size  = [28,microscope_slide_size.y+3,11];
slider_linear_bearing_radius = 4.6;
slider_linear_bearing_width = 12;
slider_linear_bearing_length = 16;
slider_handle_length = 90;
slider_angle = 50;


//Clamp Parameters
clamp_height = 5;
pushpad_width = 5;
pushpad_depth = 4;
endpiece_width = 10;
endpiece_depth = 5;

//Slider clamp parameters
clamp_centre = -10; 
//-1.6 for 80 degrees
//-3.1 for 70 degrees
//-4.9 for 60 degrees
//-7.6 for 50 degrees
//-10 for 40 degrees
//-15 for 30 degrees

//Half distance between clamp connections on the slider
clamp_position = 15.6;


//Motor
motor_size = [12,25,10];
nema_17_size = [39.5,42.3,42.3];

// Raspberry Pi Zero base
base_size = [105,75, box_size.z-lid_thickness];
base_screw_offset = 5;


//Actuator 
actuator_size=[15,14,6];
actuator_wall_thickness=1;
actuator_lid_thickness=5;
actuator_bodge_factor=[1,1,1];
actuator_offset = [microscope_slide_offset+microscope_slide_size.x+microscope_slide_bodge_factor+actuator_bodge_factor.x+5,(microscope_slide_size.y+microscope_slide_bodge_factor)-2*5.7-actuator_size.y-2*actuator_bodge_factor.y+19.5,0];


function tiny() = 0.05;
