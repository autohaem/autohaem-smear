include <shared_parameters.scad>;

version_numstring ="v2.0.0";

function tiny() = 0.05;

//Main box
radius_of_curvature = 1;
box_size = [110,50,40];

//Wedge sides
wedge_size = [box_size.x,10,box_size.z];

rod_height = 8;

//Living hinge
hinge_box_size  = [25,box_size.y,rod_height*2];
hinge_box_screws = microscope_slide_size.y/2;


slider_angle = 30;

//Rod rest


rod_rest_size = [10,10,rod_height];
rod_rest_cylinder_length = 50;
rod_rest_x_pos = microscope_slide_offset;


//Rod
rod_length=100;
rod_radius = 3;
rod_radius_slide_bodge = 0.25;
rod_radius_bodge = 0.05;
distance_between_rods = box_size.y-10;


slider_handle_length = 90;

// Rod Handle
rod_handle_length = 8;
rod_handle_radius = 5;
rod_handle_height = hinge_box_size.z/2;

