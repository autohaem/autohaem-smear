include <shared_parameters.scad>;
include <./logo/logo.scad>;
use <utilities.scad>;
//Main box

//Microscope slide
module microscope_slide(){
cube([microscope_slide_size.x+microscope_slide_bodge_factor,microscope_slide_size.y+microscope_slide_bodge_factor,microscope_slide_size.z+microscope_slide_bodge_factor]);
}


module logo(version_numstring){
    render(6){
        translate([-0.01,0,30])rotate([90,0,90])resize([25,0,0.5],auto=true)surface(file = "logo/logo_reflect.png", center = true);
    }
    translate([-0.01,-8,13])rotate([90,0,90])resize([25,0,0.5],auto=true)oshw_logo_and_text(version_numstring);
}


module each_base_hole(){
     repeat([0,box_size.y,0],2) translate([box_size.x/2-base_screw_offset,0,0])reflect([1,0,0])translate([box_size.x/2-base_screw_offset,0,0])children();
}

module base_nut_traps(){
    translate([base_screw_offset,0,0])each_base_hole(){
        hull(){
            translate([0,0,2])nut(d=3.2, h = 4,shaft=false);
            translate([10,0,2])nut(d=3.2, h = 4,shaft=false);
        }
        cylinder(h=10,r=3/2*1.05*(1.22+1)/2,$fn=50);
    }
}

module slide_access_dips(){
    translate([(microscope_slide_size.x+microscope_slide_bodge_factor)/2,(microscope_slide_size.y+microscope_slide_bodge_factor)/2,17.5]){
        reflect([0,1,0]){
            translate([0,-(microscope_slide_size.y+microscope_slide_bodge_factor)/2,0])difference(){
                sphere(r=20,$fn = 100);
                translate([0,25,0])cube(50,center=true);
            }
        }
    }
}

module microscope_slide_and_dips(){
    slide_access_dips();
    microscope_slide();
}




