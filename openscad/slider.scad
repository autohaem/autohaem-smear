include <smear_parameters.scad>;
use <utilities.scad>;

//Glass slide recess that is subtracted from the model
module slide(){
    rotate([0,slider_angle,0])cube([microscope_slide_size.x,microscope_slide_size.y+0.5,2],center = true);
}

//Handle, which part that the person will by gripping
module slider_handle(){
    translate([-slider_handle_length/2,0,0])cube([slider_handle_length,slider_size.z/2,slider_size.z],center = true);
    translate([-slider_handle_length,(slider_size.y-4)/2,0.7])rotate([180,0,0])cylinder_with_45deg_top(r = slider_size.z/2,h = slider_size.y-4,$fn=100);
}

//Housing for the microscope slide
module smear_slider(){
    intersection(){
        cube(slider_size,true);
        rotate([0,slider_angle,0])cube([100,slider_size.y,microscope_slide_size.z+6],center=true);
    }
}

module smear_plus_slider(){
    intersection(){
        cube(slider_size,true);
    }
}


//Side cuboids for the rod housing
module linear_bearings(){
    reflect([0,1,0])translate([0,slider_size.y/2+slider_linear_bearing_box_width/2,0])cube([slider_linear_bearing_box_length,slider_linear_bearing_box_width,slider_size.z+1],center=true);
}

//Cylinders with which the bearings will be inserted into, then housing the rods
module slider_holes(){
    translate([-(slider_size.x+10),distance_between_rods/2,0.5])rotate([0,90,0])cylinder(r = slider_linear_bearing_radius, h =999, $fn=100);
    translate([-(slider_size.x+10),-distance_between_rods/2,0.5])rotate([0,90,0])cylinder(r = slider_linear_bearing_radius, h =999, $fn=100);
}



//combination of all the different modules to provide the model
module clamp_slider(){
    difference(){
        union(){
            smear_slider();
            slider_handle();
            translate([0,0,0.5])linear_bearings();
        }
        slide();
        slider_holes();
    }
}

clamp_slider();
