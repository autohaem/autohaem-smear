use <shared_main_body_objects.scad>;
use <utilities.scad>;
include <smear_parameters.scad>;

module smear_each_bottom_corner(){
    translate([radius_of_curvature,radius_of_curvature-wedge_size.y,0]) repeat([box_size.x-2*radius_of_curvature,0,0],2) repeat([0,box_size.y-radius_of_curvature*2+wedge_size.y*2,0],2) children();
}
module smear_each_top_corner(){
    translate([radius_of_curvature,radius_of_curvature,box_size.z])repeat([box_size.x-2*radius_of_curvature,0,0],2) repeat([0,box_size.y-2*radius_of_curvature,0],2) children();
}

module smear_main_box(){
    hull(){
        smear_each_bottom_corner() cylinder(r=radius_of_curvature,h=tiny(),$fn=50);
        smear_each_top_corner() cylinder(r=radius_of_curvature,h=tiny(),$fn=50);
    }
}

//Rod rest
module rod_rest(){
    translate([0,-rod_rest_size.y/2,0])difference(){
            translate([0.5,0.5,0])minkowski(){
                cube(rod_rest_size-[2*0.5,2*0.5,0]);
                cylinder(r=0.5, h=tiny(),$fn=100);
            }
        translate([-5, rod_rest_size.y/2, (hinge_box_size.z+rod_radius_slide_bodge)/2]){
            rotate([0,90,0]){cylinder(h = rod_rest_cylinder_length, r = rod_radius+rod_radius_slide_bodge,$fn = 100);}
        }
        translate([(rod_rest_size.x-magnet_size.x)/2,(rod_rest_size.y-magnet_size.y)/2,(hinge_box_size.z/2-1)-magnet_size.z-rod_radius])cube(magnet_size+[0.5,10,0.5]);   
    }
}

module hinge_box_screws(){
    translate([0,box_size.y/2,0])reflect([0,1,0]){
        translate([box_size.x-hinge_box_size.x/4,microscope_slide_size.y/2,box_size.z-5])nut_from_bottom(d=3);
    }
}


module main_body(){
    difference(){
        union(){
            smear_main_box(); //main box
            translate([0,box_size.y/2,0])reflect([0,1,0]){
                translate([rod_rest_x_pos,distance_between_rods/2,box_size.z])rod_rest();
            }
        }
        translate([microscope_slide_offset, box_size.y/2-(microscope_slide_size.y+microscope_slide_bodge_factor)/2,(box_size.z-microscope_slide_size.z)])
        {
            microscope_slide_and_dips();
        }
        hinge_box_screws(); // screw holes to attach hinge box
        translate([0,box_size.y/2,0])logo(version_numstring);
    }
}
main_body();
