include <./smear_parameters.scad>;
use <utilities.scad>;


module hinge_box(){
    translate([radius_of_curvature,radius_of_curvature,0])hull(){
        repeat([hinge_box_size.x-2*radius_of_curvature,0,0],2)repeat([0,hinge_box_size.y-2*radius_of_curvature,0],2)cylinder(r=radius_of_curvature,h=hinge_box_size.z,$fn=50);
    }
    //cube(hinge_box_size);
}

module hinge_holes(){
    translate([hinge_box_size.x*3/4,(hinge_box_size.y+distance_between_rods)/2,hinge_box_size.z/2])rotate([0,90,0])cylinder(r = rod_radius+rod_radius_bodge, h= 50,$fn=100);
    translate([hinge_box_size.x*3/4,(hinge_box_size.y-distance_between_rods)/2,hinge_box_size.z/2])rotate([0,90,0])cylinder(r = rod_radius+rod_radius_bodge, h= 50,$fn=100);
    translate([hinge_box_size.x/4,(hinge_box_size.y+microscope_slide_size.y)/2,0])union(){
        translate([0,0,4])hole_from_bottom(r=2, h=50, base_w=7, delta_z=0.3, layers=4, big_bottom=false);
        translate([0,0,-0.29])cylinder(d = 7, h=4, $fn=50);
    }
    translate([hinge_box_size.x/4,(hinge_box_size.y-microscope_slide_size.y)/2,0])union(){
        translate([0,0,4])hole_from_bottom(r=2, h=50, base_w=7, delta_z=0.3, layers=4, big_bottom=false);
        translate([0,0,-0.29])cylinder(d = 7, h=4, $fn=50);
    }
    translate([hinge_box_size.x/2,0,0.5])cube([1,hinge_box_size.y,50]);
}

module hinge_component(){
    difference(){
        hinge_box();
        hinge_holes();
    }
}

hinge_component();
