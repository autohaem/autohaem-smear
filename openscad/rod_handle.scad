include <smear_parameters.scad>;

module rod_handle(){
    difference(){
        union(){
            rotate_extrude(angle=180,$fn=100)translate([distance_between_rods/2-rod_handle_radius,0,0])square([rod_handle_radius*2,rod_handle_height]);
            translate([-distance_between_rods/2,0,0])cylinder(r= rod_handle_radius, h = rod_handle_height,$fn=50);
            translate([distance_between_rods/2,0,0])cylinder(r= rod_handle_radius, h = rod_handle_height,$fn=50);
        }
    translate([-distance_between_rods/2,0,rod_handle_length/4])cylinder(r = rod_radius+rod_radius_bodge,h=rod_handle_length, $fn=100);
    translate([distance_between_rods/2,0,rod_handle_length/4])cylinder(r = rod_radius+rod_radius_bodge,h=rod_handle_length, $fn=100);
    }
}


rod_handle();