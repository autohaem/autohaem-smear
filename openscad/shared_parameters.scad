

//Microscope slide
microscope_slide_bodge_factor = 0.5;
microscope_slide_size = [76,26,1];
microscope_slide_offset = 3;


// Slider
slider_size  = [28,microscope_slide_size.y+3,11];
slider_linear_bearing_radius = 4.6;
slider_linear_bearing_box_width = 12;
slider_linear_bearing_box_length = 16;
magnet_size = [7.2,2.7,2.5];

//Clamp Parameters
clamp_height = 5;
pushpad_width = 5;
pushpad_depth = 4;
endpiece_width = 10;
endpiece_depth = 5;

//Slider clamp parameters
//clamp_centre = -10; 
//-1.6 for 80 degrees
//-3.1 for 70 degrees
//-4.9 for 60 degrees
//-7.6 for 50 degrees
//-10 for 40 degrees
//-15 for 30 degrees

//Half distance between clamp connections on the slider
//clamp_position = 15.6;




