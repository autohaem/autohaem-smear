include <parameters.scad>;
use <slider_wclamp.scad>;
use <clamp.scad>;
use <utilities.scad>;

translate([clamp_centre,0,9.6])rotate([180,0,180])clamp_total();
clamp_slider();