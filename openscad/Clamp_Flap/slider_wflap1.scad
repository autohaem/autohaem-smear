include <parameters.scad>;
use <utilities.scad>;
use <slider_wflap.scad>;

rotate([-90,0,0])difference(){
    slider_full();
    translate([-100,0,-60])cube(150);
}
