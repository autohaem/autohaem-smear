include <parameters.scad>;
use <utilities.scad>;

//Glass slide recess that is subtracted from the model
module slide(){
    rotate([0,slider_angle,0])cube([microscope_slide_length,microscope_slide_width+0.5,2],center = true);
    difference(){   
        translate([5,0,-7])cube([2,microscope_slide_width+0.5,5],true);
        translate([3.5,0,-5.15])rotate([90,0,0])cylinder(microscope_slide_width+0.5,r = 0.7,center = true,$fn=30);
    }
}

//Handle, which part that the person will by gripping
module slider_handle(){
    translate([-38,0,0])cube([104,slider_size.z/2,slider_size.z],center = true);
    translate([-90,(slider_size.y-4)/2,0])rotate([90,0,0])cylinder(r = slider_size.z/2,h = slider_size.y-4,$fn=100);
    //The cylinder extended through the front of the slider to provide space for a bolt
    translate([14,0,0])rotate([90,0,0])cylinder(slider_size.z/2,r = 5.5,center = true);
}

//Housing for the microscope slide
module slider(){
    cube([slider_size.x, slider_size.y, slider_size.z],true);
}

//Side cuboids for the rod housing
module linear_bearings(){
    reflect([0,1,0])translate([0,slider_size.y/2+slider_linear_bearing_width/2,0])cube([slider_linear_bearing_length,slider_linear_bearing_width,slider_size.z],center=true);
}

//Cylinders with which the bearings will be inserted into, then housing the rods
module slider_holes(){
    translate([-(slider_size.x+10),distance_between_rods/2,0])rotate([0,90,0])cylinder(r = slider_linear_bearing_radius, h =999, $fn=100);
    translate([-(slider_size.x+10),-distance_between_rods/2,0])rotate([0,90,0])cylinder(r = slider_linear_bearing_radius, h =999, $fn=100);
}

//Gap made for the flap that will be opposing the motion
module gap_for_flap(){
    difference(){
        cube([5.5,microscope_slide_size.y+0.5,5],center=true);
        translate([-1,0,-2.5])rotate([0,45,0])cube([microscope_slide_size.x,microscope_slide_size.y+1,5],true);
    }
}

//Flap itself that will provide the resistive force that is needed in order to keep the glass slide held in place for the duration of the smear
module flap(){
    translate([1.4,0,-0.2])
    hull(){
        translate([-0.9,0,1.3])rotate([0,-25,0])cube([0.4,microscope_slide_size.y-0.5,4],true);
        translate([0,0,0])rotate([90,0,0])cylinder(h = microscope_slide_size.y-0.5, r = 0.4, $fn = 20, center = true);
    }
}   

//Hole for a bolt to clamp the two halves of the print together
module bolt_hole() {
    rotate ([90,0,0]) union(){
        cylinder (15, r=1.75, center=true, $fn=30);
        translate([0,0,-6.5])cylinder(5,r = 3.4,$fn=6);
    }
}

//module assembling the complete slider
module slider_full (){
    union(){
        difference(){
            union(){
                slider();
                slider_handle();
                linear_bearings();
            }
            slide();
            slider_holes();
            gap_for_flap();
            translate([-20,0,0])bolt_hole();
            translate([-75,0,0])bolt_hole();
            translate([14,0,0])bolt_hole();
        }     
        flap();
    }
}

slider_full();
