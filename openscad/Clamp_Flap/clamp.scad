include <parameters.scad>;
use <utilities.scad>;

//Link on the inner side of the join between end_piece and diagonal
module cylinder_link_inner (){
    difference(){
        translate([-4.7,15,0])rotate([0,0,45])cube([1.8,1,clamp_height],true);
        translate([-5.3,14.45,0])cylinder(h=clamp_height+1,r=0.3,center = true,$fn = 50);
        translate([-5,14,0])cube([1,1,clamp_height+1],true);
    }
}

//Link on the outer side of the join between end_piece and diagonal
module cylinder_link_outer(){
    difference(){
        translate([-5,16,0])cube([1,1,clamp_height],true);
        translate([-6.5,16.5,0])cylinder(h=clamp_height + 1,r=1.5,center=true, $fn=50);
    }
}

//End piece of the clamp, to have the guides on top
module end_piece(){ 
    translate([-5,microscope_slide_size.y/2,-2.5])cube([endpiece_width,endpiece_depth,clamp_height]);   
}

//Diagonals between pushpad and an end piece. Separate models due to the rotation
module diagonal_one (){
    translate([-11,microscope_slide_size.y/4+2,0])rotate([0,0,50])cube([20,0.5,clamp_height],true);
    translate([-10.5,microscope_slide_size.y/4+2.5,0])rotate([0,0,50])cube([9,1.5,clamp_height],true);
}

module diagonal_two (){
    translate([11,microscope_slide_size.y/4+2,0])rotate([0,0,-50])cube([20,0.5,clamp_height],true);
    translate([10.5,microscope_slide_size.y/4+2.5,0])rotate([0,0,-50])cube([9,1.5,clamp_height],true);
}

//Pushpad, being the part that is pushed to change the distance between the end pieces. Also houses the connector to the main slide
module push_pad (){
    cube([pushpad_depth,pushpad_width,clamp_height],true);
    difference(){
        translate([0.5,3,0])cube([1,1,clamp_height],true);
        translate([0.8,3.5,0])cylinder(h=clamp_height+1,r=1,center = true,$fn = 20);
    }
    difference(){
        translate([-0.5,2.4,0])rotate([0,0,45])cube([1,1.5,clamp_height],true);
        translate([-1.15,2.9,0])cylinder(h=clamp_height+1,r=0.4,center = true,$fn = 20);
    }
}

//Guide to constrain the glass slide to a certain position to minimalise any slipping that may occur
module guide (){
    difference(){
        translate([0,microscope_slide_size.y/2-0.5,0])cube([endpiece_width,1.5,clamp_height],true);
        rotate([0,slider_angle,0])cube([microscope_slide_size.x,microscope_slide_size.y,microscope_slide_size.z+0.5],true);
        translate([7.5,0,0])rotate([0,slider_angle,0])cube([microscope_slide_size.x,microscope_slide_size.y,4],true);
        translate([-7.5,0,0])rotate([0,slider_angle,0])cube([microscope_slide_size.x,microscope_slide_size.y,4],true);
    }
}

//construction of the overall clamp shape
module clamp_shape(){
    end_piece();
    cylinder_link_inner();
    mirror([1,0,0])cylinder_link_inner();
    cylinder_link_outer();
    mirror([1,0,0])cylinder_link_outer();
    
    diagonal_one();
    diagonal_two();
    translate([16,0,0])push_pad();
    mirror([1,0,0])translate([16,0,0])push_pad();
    guide();
}

//Connection piece to the slider, allowing for a slide on
module clamp_holder (){
    difference(){
        union(){
            translate([1,0,0])sphere(d = 2, true,$fn = 20);
            translate([1.55,0,-1])rotate([0,-30,0])cylinder(h = 1,r = 1, $fn = 20);
            translate([0,0,-1])cube(2,true);
            rotate([0,90,0])cylinder(h = 2,r = 1,$fn = 20, center=true);
        }
        translate([-0.4,0,-0.5])rotate([0,60,0])cube([4,4,2],true);
    }
}

module clamp_total (){
    union(){
        translate([15,0,3])clamp_holder();
        translate([-15,0,3])rotate([0,0,180])clamp_holder();
        clamp_shape();
        mirror([0,1,0])clamp_shape();
    }
}
clamp_total();

//Module that allows the clamp scad file to be attached to the slider model
module clamp_holder (){
    difference(){
        hull(){
            translate([0,0,0.3])rotate([90,0,0])cylinder(h = 5,d = 2, center = true,$fn = 20);
            translate([-1,0,-1])cube([2,5,3],true);
            translate([-1,0,0.3])rotate([90,0,0])cylinder(h = 5,d = 2, center = true,$fn = 20);
            translate([-3.5,0,-2])rotate([0,30,0])cube([2,5,3],true);
        }
        translate([0.6,0,-0.5])rotate([0,30,0])cube([2,6,3],true);
    }
}