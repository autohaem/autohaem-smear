module oshw_logo(){
    linear_extrude(1) translate([-17.5,-16]) resize([35,0],auto=true) import("logo/oshw_gear.dxf");
}


module oshw_logo_and_text(text=""){
    union(){
        oshw_logo();
        
        translate([100,-7,0]) mirror([1,0,0]) linear_extrude(1){
            text(text, size=14, font="Calibri", halign="left");
        }
    }
}